'use strict';
module.exports = function(app) {
    var machines = require('../controllers/machines_controller');
    var main = require('../controllers/documentation_controller');

    // Machine Routes
    app.route('/')
        .get(main.index);
    
    app.route('/machines')
        .get(machines.index)
        .post(machines.create);

    app.route('/machines/new')
        .get(machines.new);

    app.route('/machines/:machineId')
        .get(machines.show)
        .put(machines.update)
        .delete(machines.destroy);

    app.route('/machines/:machineId/edit')
        .get(machines.edit);
};