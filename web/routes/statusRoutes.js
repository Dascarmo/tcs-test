'use strict';
module.exports = function(app) {
    var status = require('../controllers/status_controller');

    // Machine Routes
    app.route('/')
        .get(status.index);
    
    app.route('/status')
        .get(status.index)
        .post(status.create);

    app.route('/status/new')
        .get(status.new);

    app.route('/status/:statusId')
        .get(status.show)
        .put(status.update)
        .delete(status.destroy);

    app.route('/status/:statusId/edit')
        .get(status.edit);
};