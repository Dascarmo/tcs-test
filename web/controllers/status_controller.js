'use strict';

var mongoose = require('mongoose'),
HttpStatus = require('http-status-codes'),
Status = mongoose.model('Status');

exports.index = function(req, res) {
    Status.find({}).exec( function(err, status) {
        if (err)
            res.send(err);
        res.render('status/index', {status: status});
    });
};

exports.show = function(req, res) {
    Status.findOne({_id: req.params.statusId}).exec( function(err, status) {
        if (err)
            res.send(err);
        res.render('status/show', {status: status});
    });
};

exports.new = function(req, res) {
    var status = new Status();
    res.render('status/new', {status: status, action: "/status", method: "POST"});
};

exports.create = function(req, res) {
    var status = new Status(req.body);
    status.save(function(err, new_status) {
        if (err)
            res.send(err);
        else
            res.writeHead(301,
                {Location: '/status/'+new_status._id}
            );
            res.end();
    });
};

exports.edit = function(req, res) {
    Status.findOne({_id: req.params.statusId}).exec( function(err, status) {
        if (err)
            res.send(err);
        res.render('status/edit', {status: status, action: "/status/"+status._id, method: "PUT"});
    });
};

exports.update = function(req, res) {
    Status.findOneAndUpdate({_id: req.params.statusId}, req.body).exec(function(err, status) {
        if (err)
            res.send(err);
        res.writeHead(301,
            {Location: '/status/'+status._id}
        );
        res.end();
    });
};

exports.destroy = function(req, res) {
    Status.findOneAndRemove({_id: req.params.statusId}, req.body).exec(function(err, status) {
        if (err)
            res.send(err);
        res.writeHead(301,
            {Location: '/status'}
        );
        res.end();
    });
};