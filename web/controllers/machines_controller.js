'use strict';

var mongoose = require('mongoose'),
HttpStatus = require('http-status-codes'),
Machine = mongoose.model('Machine');
StatusLog = mongoose.model('StatusLog');
Status = mongoose.model('Status');

exports.index = function(req, res) {
    Machine.find({}).populate({path: 'status'}).exec( function(err, machines) {
        if (err)
            res.send(err);
        res.render('machines/index', {machines: machines});
    });
};

exports.show = function(req, res) {
    Machine.findOne({_id: req.params.machineId}).populate([{path: 'status'},{path: 'status_logs', options: {sort: {created_at: -1} } }]).exec( function(err, machine) {
        if (err)
            res.send(err);
        res.render('machines/show', {machine: machine});
    });
};

exports.new = async function(req, res) {
    var machine = new Machine();
    res.render('machines/new', {machine: machine, action: "/machines", method: "POST", status: await Status.find().exec() });
};

exports.create = function(req, res) {
    var machine = new Machine(req.body);
    machine.save(function(err, new_machine) {
        if (err)
            res.send(err);
        else
            res.writeHead(301,
                {Location: '/machines/'+new_machine._id}
            );
            res.end();
    });
};

exports.edit = function(req, res) {
    Machine.findOne({_id: req.params.machineId}).populate({path: 'status_logs', options: {sort: {created_at: -1} } }).exec( function(err, machine) {
        if (err)
            res.send(err);
        res.render('machines/edit', {machine: machine, action: "/machines/"+machine._id, method: "PUT"});
    });
};

exports.update = function(req, res) {
    Machine.findOneAndUpdate({_id: req.params.machineId}, req.body).exec(function(err, machine) {
        if (err)
            res.send(err);
        res.writeHead(301,
            {Location: '/machines/'+machine._id}
        );
        res.end();
    });
};

exports.destroy = function(req, res) {
    Machine.findOneAndRemove({_id: req.params.machineId}, req.body).exec(function(err, machine) {
        if (err)
            res.send(err);
        res.writeHead(301,
            {Location: '/machines'}
        );
        res.end();
    });
};