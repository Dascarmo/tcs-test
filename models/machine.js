'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var Machine = new Schema({
    status: {
        type: Schema.Types.ObjectId,
        ref: 'Status',
        required: "The machine needs a status!"
    },
    name: {
      type: String,
      unique: true,
      required: 'The machine needs a name!'
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    updated_at: {
        type: Date,
        default: Date.now
    },
    status_logs: [{type: Schema.Types.ObjectId, ref: 'StatusLog'}]
});

module.exports = mongoose.model('Machine', Machine);