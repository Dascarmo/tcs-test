'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var Status = new Schema({
    name: {
        type: String,
        unique: true,
        required: 'The status needs a name!'
    },
    code: {
      type: Number,
      unique: true,
      required: 'The status needs a code!'
    }
});

module.exports = mongoose.model('Status', Status);