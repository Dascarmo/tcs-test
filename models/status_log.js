'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var StatusLog = new Schema({
    machineId: {
        type: Schema.Types.ObjectId,
        ref: 'Machine',
        required: "The status log needs a machine!"
    },
    status: {
        type: Schema.Types.ObjectId,
        ref: 'Status',
        required: "The status log needs a status!"
    },
    description: {
      type: String,
      required: 'The status log needs a description!'
    },
    created_at: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('StatusLog', StatusLog);