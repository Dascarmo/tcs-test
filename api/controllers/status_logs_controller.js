'use strict';

var mongoose = require('mongoose'),
HttpStatus = require('http-status-codes'),
StatusLog = mongoose.model('StatusLog');
Machine = mongoose.model('Machine'); 
Status = mongoose.model('Status'); 

exports.create = function(req, res) {
    var status_log = new StatusLog(req.body);
    Status.findOne({code: req.body.status}).exec(function(err, status){
        if (err)
            res.send(err);
        if (status){
            Machine.findOne({_id: status_log['machineId']}, function(err, machine){
                if (err)
                    res.send(err)
                else if (!machine)
                    res.send({error: "not found"})
                else
                    status_log.save(function(err, statusLog) {
                        if (err)
                            res.send(err);
                        else{
                            Machine.findOneAndUpdate({_id: statusLog['machineId']}, {status: statusLog['status']}, {new: true}, function(err, machine) {
                                if (err)
                                    res.send(err);
                                else{
                                    machine.status_logs.push(statusLog);
                                    machine.save();
                                    res
                                    .status(HttpStatus.OK)
                                    .json(statusLog);
                                }
                            });
                        }
                    });
            });
        }
        else
            res
            .status(HttpStatus.UNPROCESSABLE_ENTITY)
            .json({error: "Código de status inválido!"});
    
    });
};

exports.simulate = function(req, res) {
    Status.find({}).exec(async function(err, status){
        if(err)
            res.send(err)
        var simulation_logs = new Array(),
            machineCount = await Machine.countDocuments().exec(),
            statusCount = await Status.countDocuments().exec(),
            i;
        for(i=0 ; i < req.body.quantity ; i++){
            var machine = await Machine.findOne({}).skip(Math.floor(Math.random() * machineCount)).exec();
            var status = await Status.findOne({}).skip(Math.floor(Math.random() * statusCount)).exec();
            var new_simulation_log = new StatusLog({
                machineId: machine._id,
                status: status._id,
                description: "Simulated Log."
            })
            simulation_logs.push(new_simulation_log);
        }
        StatusLog.insertMany(simulation_logs, async function (err, status_logs) {
            if (err)
                res.send(err)
            for(i = 0 ; i < status_logs.length ; i++){
                await Machine.findOneAndUpdate({_id: status_logs[i]['machineId']}, {status: status_logs[i]['status']}, {new: true}, function(err, machine) {
                    if (err)
                        res.send(err);
                    else{
                        machine.status_logs.push(status_logs[i]);
                        machine.save();
                    }
                });
            }
            res
            .status(HttpStatus.OK)
            .json({success: true});
        });
    });
};