'use strict';

var mongoose = require('mongoose'),
HttpStatus = require('http-status-codes'),
Machine = mongoose.model('Machine');
StatusLog = mongoose.model('StatusLog');

exports.index = function(req, res) {
    Machine.find({}).populate({path: 'status_logs', select: ['description'], options: {sort: {created_at: -1}, limit: 1}}).exec( function(err, machines) {
        if (err)
            res.send(err);
        res
        .status(HttpStatus.OK)
        .json(machines);
    });
};

exports.show = function(req, res) {
    Machine.findOne({_id: req.params.machineId}).exec(function(err, machine){
        if (err)
            res.send(err);
        else
            res
            .status(HttpStatus.CREATED)
            .json(machine);
    });
};

exports.create = function(req, res) {
    var machine = new Machine(req.body);
    machine.save(function(err, new_machine) {
        if (err)
            res.send(err);
        else
            res
            .status(HttpStatus.CREATED)
            .json({
                name: new_machine.name,
                status: new_machine.status
            });
    });
};

exports.destroy = function(req, res) {
    Machine.remove({
        _id: req.params.machineId
    }, function(err, machine) {
        if (err)
            res.send(err);
        res.json({ message: 'Máquina removida com sucesso!' });
    });
};

exports.reset = function(req, res) {
    Machine.remove({}, function(err, machine) {
        if (err)
            res.send(err);
        res.json({ message: 'Termos removidos com sucesso!' });
    });
  };