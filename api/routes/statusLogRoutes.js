'use strict';
module.exports = function(app) {
    var status_logs = require('../controllers/status_logs_controller');

    // Status Logs Routes
    app.route('/api/v1/status_logs')
        .post(status_logs.create);
    app.route('/api/v1/status_logs/simulate')
        .post(status_logs.simulate);
};