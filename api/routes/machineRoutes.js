'use strict';
module.exports = function(app) {
    var machines = require('../controllers/machines_controller');

    // Machine Routes
    app.route('/api/v1/machines')
        .get(machines.index)
        .post(machines.create)
        .delete(machines.reset);
    app.route('/api/v1/machines/:machineId')
        .get(machines.show)
        .delete(machines.destroy);
};