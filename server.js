var express = require('express'),
    app = express(),
    port = process.env.PORT || 3000;
    path = require('path');
    mongoose = require('mongoose'),
    Machine = require('./models/machine'),
    StatusLog = require('./models/status_log'),
    Status = require('./models/status'),
    methodOverride = require('method-override'),
    exphbs  = require('express-handlebars'),
    bodyParser = require('body-parser');
    

app.use(bodyParser.json());
app.use(bodyParser.urlencoded())
app.use(methodOverride(function (req, res) {
    if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // look in urlencoded POST bodies and delete it
        var method = req.body._method
        delete req.body._method
        return method
    }
}))

app.use(express.static(__dirname + '/node_modules'));
app.set('views', path.join(__dirname, '/web/views'));
// app.engine('html', require('ejs').renderFile);
// app.set('view engine', 'html');
app.engine('hbs', exphbs({
    defaultLayout: '../../web/views/layouts/main', 
    extname: '.hbs',
    helpers: {
        if_eq: function(a, b, opts) {
            if(a == b) // Or === depending on your needs
                return opts.fn(this);
            else
                return opts.inverse(this);
        }
    }
}));
app.set('view engine', 'hbs');

// mongoose instance connection url connection
mongoose.Promise = global.Promise;
// mongoose.connect('mongodb://localhost/TCSChallenge'); 
mongoose.connect('mongodb://daniel:tcs-test1@ds257752.mlab.com:57752/heroku_41hmfgbp'); 


var apiStatusRoutes = require('./api/routes/statusLogRoutes'); //importing route
apiStatusRoutes(app); //register the route
var apiMachineRoutes = require('./api/routes/machineRoutes'); //importing route
apiMachineRoutes(app); //register the route
var webMachineRoutes = require('./web/routes/machineRoutes'); //importing route
webMachineRoutes(app); //register the route
var webStatusRoutes = require('./web/routes/statusRoutes'); //importing route
webStatusRoutes(app); //register the route

app.listen(port);

console.log('TCS Challenge RESTful API server started on: ' + port);