# tcs-test

Aplicação web para o teste da TCS.

![nodei.co](https://nodei.co/npm/tcs-test.png?downloads=true&downloadRank=true&stars=true)


## Instalação

`npm install --save tcs-test`


## Scripts

 - **npm run test** : `echo "Error: no test specified" && exit 1`
 - **npm run dev** : `nodemon server.js`
 - **npm run start** : `node server.js`

## Dependências

Pacote | Versão | Dev
--- |:---:|:---:
[bootstrap](https://www.npmjs.com/package/bootstrap) | ^4.1.3 | ✖
[ejs](https://www.npmjs.com/package/ejs) | ^2.6.1 | ✖
[express](https://www.npmjs.com/package/express) | ^4.16.3 | ✖
[express-handlebars](https://www.npmjs.com/package/express-handlebars) | ^3.0.0 | ✖
[express-partial](https://www.npmjs.com/package/express-partial) | ^0.1.0 | ✖
[handlebars](https://www.npmjs.com/package/handlebars) | ^4.0.12 | ✖
[http-status-codes](https://www.npmjs.com/package/http-status-codes) | ^1.3.0 | ✖
[jquery](https://www.npmjs.com/package/jquery) | ^3.3.1 | ✖
[method-override](https://www.npmjs.com/package/method-override) | ^3.0.0 | ✖
[mongoose](https://www.npmjs.com/package/mongoose) | ^5.2.14 | ✖
[popper](https://www.npmjs.com/package/popper) | ^1.0.1 | ✖
[popper.js](https://www.npmjs.com/package/popper.js) | ^1.14.4 | ✖
[node-readme](https://www.npmjs.com/package/node-readme) | ^0.1.9 | ✔
[nodemon](https://www.npmjs.com/package/nodemon) | ^1.18.4 | ✔


## Autor

Daniel Carmo.

## Licença

 - **ISC** : http://opensource.org/licenses/ISC

## Disponibilidade

O link para se acessar o sistema é esse: [teste-tcs-site](https://frozen-escarpment-99456.herokuapp.com).

## Enunciado

Um cliente solicita a construção de uma aplicação WEB para controlar o status de cada máquina do seu parque industrial. Cada máquina possui um nome, e sensores acoplados a cada máquina irão periodicamente enviar à aplicação construída o status de cada máquina (eventos de status). Esses eventos de status são enviados informando o código da máquina a que se referem e o código do status (valor numérico). Eles devem ser recebidos pela aplicação WEB e armazenados no banco de dados.

## Objetivo

Esta aplicação tem como objetivo o monitoramento de estados de máquinas. Os requisitos são:

- O sistema deve permitir funcionalidade para cadastro, edição e exclusão de máquinas (campos da máquina: nome). [**concluído**]
- O sistema deve permitir funcionalidade para cadastro, edição e exclusão de status (campos do status: código e nome). [**concluído**]
- O sistema deve possuir uma tela inicial em que mestre a lista de todas as máquinas cadastradas, seus respectivos nomes e o seu status atual (o status atual é definido como o último evento de status recebido para aquela máquina). [**concluído**]
- O sistema deve possuir um simulador que, a cada intervalo de tempo, gere um evento de status aleatório para cada máquina cadastrada no banco de dados (campos do evento: data e código do status).  [**parcialmente concluído**]
- O sistema deve possuir funcionalidade para que o usuário configure a periodicidade com que os eventos aleatórios serão gerados.  [**parcialmente concluído**]

## Funcionalidades

O sistema possui duas vertentes (WEB e API). A vertente a ser documentada é a API (chamadas RESTFull). São elas:

### Status Logs

Chamadas destinadas ao objeto de logs de estado. São elas:

#### Create Status Log

- Método: POST
- url: https://frozen-escarpment-99456.herokuapp.com/api/v1/status_log
- Parâmetros: machineId(string, obrigatório), status(number, obrigatório), description(string, opcional)
- Descrição: Chamado para o registro de um log. Devem ser passados o id da máquina à qual pertence o log e o seu código (previamente registrado no sistema WEB). Após a criação do log, o sistema ira alterar o estado da máquina caso seja diferente do último log a ser registrado antes deste.

Response 200

> { "_id": "5b9eebe67a7b2a481971f622",
    "machineId": "5b9eea2ef3dcb43e56583037",
    "status": "5b9eea21f3dcb43e56583035",
    "description": "Simulated Log.",
    "created_at": "2018-09-16T23:48:54.877Z" }

Response 422

> { "error": "Código de status inválido!" }

#### Simulate

- Método: POST
- url: https://frozen-escarpment-99456.herokuapp.com/api/v1/status_log
- Parâmetros: quantity(number, obrigatório)
- Descrição: Chamada destinada para a simulação da coleta de logs. O parametro `quantity` deverá receber a quantidade de logs a serem registrados.

Response 200

> { "success": true }

## Requisitos Não cumpridos

Para ser feito o deploy da aplicação, foi escolhido a plataforma de computação em nuvem. Foi escolhido principalmente devido ao seu custo zero para projetos pequenos. Porém, a plataforma sem custos tem as suas limitações.

Foi pensada duas soluções para os dois últimos requisitos apresentados (simulação e configuração de periodicidade de simulação). São elas:

### Solução via JavaScript

A primeira solução consiste em colocar um *script* no navegador de forma que, quando o usuário estivesse logado, requisições iriam ser feitas ao servidor de forma que os logs fossem gerados de tempos em tempos.

Essa chamada iria consultar o banco de forma a saber qual foi o tempo registrado de periodicidade e a data do último log registrado. 

O problema desta solução está no fato de que se nenhum usuário estivesse com a página do navegador aberta, a simulação não seria feita. Sendo assim, optou-se por não implementá-la.

### Solução via crontab

Crontab são tabelas de registro de atividades a serem executadas pelo servidor de tempos em tempos. Seria excelente para esta solução, uma vez que poderia se adicionar uma rotina para chamar a função de simulação de geração de logs.

A ideia seria parecida com a solução interior. Duas chamadas seriam feitas ao banco para consultar a periodicidade registrada e a data do último log armazenado. Assim, de tempos em tempos, a rotina de crontab iria realizar a solução enquanto o servidor estivesse ligado.

Porém, com a plataforma de _deploy_ escolhida, não seria possível acessar a crontab do servidor da aplicação. Porém, ainda assim, a chamada foi feita e documentada a cima. É ela: `Simulate`.